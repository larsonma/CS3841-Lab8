/**
 * This file provides the interface for writing and reading binary files for the memory debugger.
 * @file DebugFileManager.h
 * @author Mitchell Larson, Gunther Huebler
 * @date 10/31/2017
 */

#ifndef DEBUGFILEMANAGER_H_
#define DEBUGFILEMANAGER_H_

/**
 * Writes the memory at a starting address for a specified length to a specified file
 * @param input - string of <filename> <startingAddress> <lengthOfBytesToWRite>
 */
void handleFileWrite(char *);

/**
 * Writes the bytes from a file to memory starting a specified address
 * @param input <filename> <startingAddress>
 */
void handleFileRead(char*);

#endif /* DEBUGFILEMANAGER_H_ */
