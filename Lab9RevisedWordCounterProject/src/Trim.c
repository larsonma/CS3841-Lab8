/**
 * This file provides the implementation of the Trim functionality. This provides easy functions to clean
 * up words before adding them to the dictionary.
 * @file Trim.c
 * @author Mitchell Larson
 * @date 9/26/2017
 */

#include "Trim.h"
#include "memmgr.h"
#include <stdlib.h>
#include <string.h>


const int MAX_LENGTH = 32;
const int ASCII_LOWER_TO_UPPER = 32;

/**
 * This function accepts a string and removes any non-alphabetic characters. This function will not change the
 * contents of the incoming string.
 * @param text	A non-null pointer to a string
 * @return		A dynamically allocated pointer to a word. NULL if input string is NULL.
 */
char* stripNonText(const char* text){
	//check that there is a string to operate on.
	if(text != NULL){
		char trimmed[MAX_LENGTH];
		int index = 0;

		while (*text) {
			char currentChar = *text;
			if ((currentChar >= 'A' && currentChar <= 'Z')
					|| (currentChar >= 'a' && currentChar <= 'z')) {
				trimmed[index] = currentChar;
				index++;
			}
			text++;
		}
		trimmed[index] = '\0';

		char* trimmedWord = memmgr_malloc(strlen(trimmed)+1);

		if(trimmedWord)
			return memcpy(trimmedWord,trimmed,strlen(trimmed)+1);
	}

	return NULL;
}

/**
 * This function takes all lower case letters in a string and changes them to upper case. Non alphabetic characters
 * are ignored and left unchanged.
 * @param text	A non-null pointer to a string.
 * @return		A dynamically allocated pointer to string. NULL if input string is NULL.
 */
char* toCaps(const char* text){
	if(text != NULL){
		char newString[MAX_LENGTH];
		int index = 0;

		while(*text){
			char currentChar = *text;
			if (currentChar >= 'a' && currentChar <= 'z') {
				newString[index] = currentChar - ASCII_LOWER_TO_UPPER;
			}else{
				newString[index] = currentChar;
			}
			index++;
			text++;
		}
		newString[index] = '\0';

		char* returnStr = memmgr_malloc(strlen(newString)+1);
		if(returnStr)
			return memcpy(returnStr,newString,strlen(newString)+1);
	}
	return NULL;
}


