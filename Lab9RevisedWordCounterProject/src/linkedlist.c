/**
 * Linked List in C
 * @author Gunther Huebler
 * CS3841 - Lab 2
 * 9-19-2017
 *
 * This program handles the creation and usage of a linked list for general
 * elements (primitives and structs). List nodes have previous pointers but
 * the list iterators have no previous method call rendering that a currently
 * useless feature, but it is a possible addition in the future.
 *
 */
#include "linkedlist.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Initializes list, setting all parameters to NULL
 * @param list - List to initialize
 */
void ll_init(struct linkedList* list) {
	//if list is valid set initial parameters
	if(list != NULL) {
		list->head = NULL;
		list->tail = NULL;
		list->size = 0;
	}
}

/**
 * Add an element to the end of the linked list
 * @param list - List to add to
 * @param object - Pointer to object to add
 * @param size - Data size of object in bytes
 * @return
 */
bool ll_add(struct linkedList* list, const void* object, uint32_t size) {

	//return NULL if parameters are invalid
	if(list == NULL || object==NULL || size<=0) {
		return false;
	}

	//create memory space for new listNode and store in temporary pointer
	struct listNode * temp = memmgr_malloc(sizeof(struct listNode));
	temp->data = memcpy(memmgr_malloc(size), object, size);
	temp->dataSize = size;
	temp->prevNode = NULL;
	temp->nextNode = NULL;

	//increment size of list
	list->size++;

	//first element case
	if(list->head == NULL) {
		list->head = temp;
		list->tail = temp;
	}
	//general case
	else {
		list->tail->nextNode = temp;
		temp->prevNode = list->tail;
		list->tail = temp;
	}
	return true;
}

/**
 * Add and element to list at specified index
 * @param list - List to add to
 * @param object - Pointer to object to add
 * @param size - Data size of object in bytes
 * @param index - Index of list to add at
 * @return
 */
bool ll_addIndex(struct linkedList* list, const void* object, uint32_t size, uint32_t index) {

	//return NULL if parameters are invalid
	if(list == NULL || object == NULL || index < 0 || list->size <= 0 || index>list->size || size<=0) {
		return false;
	}

	//create memory space for new listNode and store in temporary pointer
	struct listNode * temp = memmgr_malloc(sizeof(struct listNode));
	temp->data = memcpy(memmgr_malloc(size), object, size);
	temp->dataSize = size;
	temp->prevNode = NULL;
	temp->nextNode = NULL;
	list->size++;

	//head case
	if(index == 0) {

		//set new node and nearby node pointers
		temp->nextNode = list->head;
		list->head->prevNode = temp;
		list->head = temp;
	}
	//general case
	else {

		//pointer for node before new node
		struct listNode * current = list->head;

		//select node immediately before index of node to add
		for(int i = 1; i<index; i++) {
			current = current->nextNode;
		}

		//set new node pointers
		temp->nextNode = current->nextNode;
		temp->prevNode = current;

		//tail case if statement
		if(current->nextNode != NULL) {
			//if not tail, set next node to new node
			current->nextNode->prevNode = temp;
		}else{
			//if tail, update list tail pointer to new node
			list->tail = temp;
		}

		//set pointer of current next node to new node
		current->nextNode = temp;
	}

	return true;
}

/**
 * Remove element from list
 * @param list - List to remove element from
 * @param index - Index of element to remove
 * @return - Return true if successful
 */
bool ll_remove(struct linkedList* list, uint32_t index) {

	//return NULL if parameters are invalid
	if(list == NULL || index < 0 || list->size <= 0 || index>=list->size) {
		return false;
	}

	//pointer for node to remove
	struct listNode * current = list->head;

	//head case
	if(index == 0) {

		memmgr_free(list->head->data);
		memmgr_free(list->head);

		list->head = list->head->nextNode;

		if(list->head != NULL) {
			list->head->prevNode = NULL;
		}else {
			list->tail = NULL;
		}
	}
	//general case
	else {

		//set current to node to be removed
		for(int i = 0; i < index; i++) {
			current = current->nextNode;
		}

		//set previous node pointer to node after removed node
		current->prevNode->nextNode = current->nextNode;

		//tail case if
		if(current->nextNode != NULL) {
			//if not tail, skip removed node in previous chain
			current->nextNode->prevNode = current->prevNode;
		}else {
			//if tail, set new tail to previous node
			list->tail = current->prevNode;
		}

		memmgr_free(current->data);
		memmgr_free(current);

	}

	//decrement size of list
	list->size--;

	return true;
}

/**
 * Get the data from list at certain index
 * @param list - List to retrieve data from
 * @param index - Index of data to retrieve
 */
void* ll_get(struct linkedList* list, uint32_t index) {

	//return NULL if parameters are invalid
	if(list == NULL || index < 0 || index >= list->size) {
		return NULL;
	}

	//temporary variable for nodes
	struct listNode * current = list->head;

	//set current to node of data to pull
	for(int i = 0; i < index; i++) {
		current = current->nextNode;
	}

	//return data
	return current->data;
}

/**
 * Clear specified list, memmgr_freeing all memory allocated data ni process
 * @param list - List to clear
 */
void ll_clear(struct linkedList* list) {

	//if parameters are valid then do stuff
	if(list != NULL && list->tail != NULL) {

		//while there are elements before tail
		while(list->tail->prevNode != NULL) {
			//decrement tail along list
			list->tail = list->tail->prevNode;

			//memmgr_free data of last element and memmgr_free last element
			memmgr_free(list->tail->nextNode->data);
			memmgr_free(list->tail->nextNode);
		}

		//memmgr_free data of first element and memmgr_free first element
		memmgr_free(list->tail->data);
		memmgr_free(list->tail);

		//set list properties to NULL
		list->head = NULL;
		list->tail = NULL;
		list->size = 0;

	}
}

/**
 * Return size of list
 * @param list - List to return size of
 * @return - 32 bit integer indicating size of list
 */
uint32_t ll_size(struct linkedList* list) {

	//return -1 if list is invalid
	if(list == NULL) {
		return -1;
	}

	//return size
	return list->size;
}

/**
 * Returns iterator for list pointing at head
 * @param list - List for iterator to point to
 * @return - Iterator
 */
struct linkedListIterator* ll_getIterator(struct linkedList* list) {

	//return null if list is invalid
	if(list == NULL) {
		return NULL;
	}

	//create memory space for iterator
	struct linkedListIterator* temp = memmgr_malloc(sizeof(struct linkedListIterator));

	//set iterator node to head
	temp->current = list->head;

	return temp;
}

/**
 * Indicates whether the iterator is pointing to a node
 * @param iter - Iterator to check
 * @return - Returns true if next element exists
 */
bool ll_hasNext(struct linkedListIterator* iter) {

	//if iter is invalid or doesn't point to valid node return false
	if(iter == NULL || iter->current == NULL) {
		return false;
	}

	return true;
}

/**
 * Returns the data at the node pointed to by iterator
 * @param iter - Iterator from which data is to be extracted
 */
void * ll_next(struct linkedListIterator* iter) {

	//if iterator has no next return NULL
	if(!ll_hasNext(iter)) {
		return NULL;
	}

	//create temporary pointer to data
	void * temp = iter->current->data;

	//set iterator to next node (null if no node)
	iter->current = iter->current->nextNode;

	//return data at current node
	return temp;
}




