/**
 * This file provides the implementation for a memory debugger. This program will work for any single process
 * application. It should be noted that this application will block execution in the main program.
 * @file DebugManager.c
 * @author Mitchell Larson, Gunther Huebler
 * @date 10/31/2017
 */

#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "DebugManager.h"
#include "DebugFileManager.h"

#define MAX_STRING (100)
static const char delim[] = " \r\n";

static jmp_buf restartEnvironment;

static void printUsage();

/**
 * This method is the signal handler that will handle the SIGSEGV segmentation fault signal. It will
 * indicate that the process attempted to access memory which was out of range.
 * @param signum This is the number of the signal.
 */
void sigsegvhandler(int signum) {
	// Indicate that an error occurred referencing memory.
	printf(
			"Error: Attempting access memory that is out of range for process %d\n",
			getpid());

	// Now go through and unblock the signal so it can be sent again.
	// Start by defining a signal set.
	sigset_t x;

	// Initialize the signal set to be empty. Then add the SIGSEGV signal to the set.
	sigemptyset(&x);
	sigaddset(&x, SIGSEGV);

	// Unblock the signal that caused us to execute this signal handler so that it can be called again.
	sigprocmask(SIG_UNBLOCK, &x, NULL);

	// Now jump back to a safe place before the erroneous dereference occurred.
	longjmp(restartEnvironment, 1);
}


/**
 * This function will start the debugger. The SIGSEGV signal will be assigned to
 * the signal handler.
 */
void startDebugger(){
	if(signal(SIGSEGV,sigsegvhandler) == SIG_ERR){
		printf("An error has occured while setting the SIGSEGV event handler. Debug start failed. Exiting");
		exit(0);
	}
}

/**
 * This function will handling printing. This function will determine the type and
 * address of the string being print and call the necessary function to print it.
 * @param input - this is a string that contains the address, type, and value for the user's command
 * 				  Separated by spaces.
 */
void handlePrints(char* input){
	if(input != NULL){
		char* type = strtok_r(NULL, delim, &input);
		char* addressStr = strtok_r(NULL, delim, &input);

		if(type != NULL && addressStr != NULL){
			unsigned long addressInt = strtoul(addressStr, NULL, 0);

			if(strcmp(type, "INT") == 0){
				printf("Address: %p Value: 0x%x\r\n",(void*)addressInt, *((int*)addressInt));
			}else if(strcmp(type, "STRING") == 0){
				printf("Address: %p Value: %s\r\n", (void*)addressInt, (char*)addressInt);
			}else if(strcmp(type, "CHAR") == 0){
				char contents = *((char*)addressInt);
				printf("Address: %p Value: %c\r\n", (void*)addressInt, contents);
			}else if(strcmp(type, "SHORT") == 0){
				short contents = *((short*)addressInt);
				printf("Address: %p Value: %hd\r\n", (void*)addressInt, contents);
			}else{
				printUsage();
			}
		}else{
			printUsage();
		}
	}else{
		printUsage();
	}
}

/**
 * This function handles setting values at addresses.
 * @param input - this is a string that contains the address, type, and value for the user's command
 * 				  Separated by spaces.
 */
void handleSets(char* input) {
	if (input != NULL) {
		char* type = strtok_r(NULL, delim, &input);
		char* addressStr = strtok_r(NULL, delim, &input);
		char* value = strtok_r(NULL, delim, &input);

		if (type != NULL && addressStr != NULL && value != NULL) {
			unsigned long addressInt = strtoul(addressStr, NULL, 0);

			if (strcmp(type, "INT") == 0) {
				*((int*) addressInt) = atoi(value);
			} else {
				printUsage();
			}
		} else {
			printUsage();
		}
	} else {
		printUsage();
	}
}

/**
 * This method will obtain the next operation from the user and invoke the necessary
 * functions. It should be noted that this function blocks until a user quits.
 */
void obtainAndPerformNextAction() {
	//string used for getting input from user
	char input[MAX_STRING + 1];

	//First set the jump for that program flow is returned here in the case of a segmentation fault.
	//If a non zero value is returned from this call, a segmentation fault occurred previously.
	setjmp(restartEnvironment);

	do {
		//get input from the user and decide the proper action to take
		fgets(input, MAX_STRING, stdin);
		char* savePrt;
		char* firstWord = strtok_r(input, delim, &savePrt);

		if(strcmp(firstWord, "PRINT") == 0){
			handlePrints(savePrt);
		}else if(strcmp(firstWord, "SET") == 0){
			handleSets(savePrt);
		}else if(strcmp(firstWord, "WRITE") == 0){
			//handleFileWrite()
			handleFileWrite(savePrt);
		}else if(strcmp(firstWord, "READ") == 0){
			handleFileRead(savePrt);
		}else if(strcmp(firstWord, "QUIT") != 0){
			//User has no idea what to do. Help them out a bit.
			printUsage();
		}

	} while(strcmp(input, "QUIT") != 0);
}

/**
 * This is a simple helper function that will indicate to a user how to use the debugger
 */
static void printUsage(){
	char* helpString =
			"Debugging usage:\n\r"
				"\tPrinting:\r\n"
					"\t\tPRINT <type> <Address>\r\n"
				"\tWriting:\r\n"
					"\t\tWRITE <filename> <Address> <Length>\r\n"
				"\tSetting:\r\n"
					"\t\tSET INT <Address> <value>\r\n"
				"\tQuit:\r\n"
					"\t\tQUIT\r\n";

	printf(helpString);
}



