/**
 * This file provides the interface for a memory debugger. This program will work for any single process
 * application. It should be noted that this application will block execution in the main program.
 * @file DebugManager.h
 * @author Mitchell Larson, Gunther Huebler
 * @date 10/31/2017
 */


#ifndef DEBUGMANAGER_H_
#define DEBUGMANAGER_H_

/**
 * This method is the signal handler that will handle the SIGSEGV segmentation fault signal. It will
 * indicate that the process attempted to access memory which was out of range.
 * @param signum This is the number of the signal.
 */
void sigsegbhandler(int signum);

/**
 * This function will start the debugger. The SIGSEGV signal will be assigned to
 * the signal handler.
 */
void startDebugger();

/**
 * This function will handling printing. This function will determine the type and
 * address of the string being print and call the necessary function to print it.
 * @param input - this is a string that contains the address, type, and value for the user's command
 * 				  Separated by spaces.
 */
void handlePrints(char* input);

/**
 * This function handles setting values at addresses.
 * @param input - this is a string that contains the address, type, and value for the user's command
 * 				  Separated by spaces.
 */
void handleSets(char* input);

/**
 * This method will obtain the next operation from the user and invoke the necessary
 * functions. It should be noted that this function blocks until a user quits.
 */
void obtainAndPerformNextAction();

#endif /* DEBUGMANAGER_H_ */
