################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/DebugFileManager.c \
../src/DebugManager.c \
../src/Lab9RevisedWordCounterProject.c \
../src/Trim.c \
../src/WordCounterApp.c \
../src/linkedlist.c \
../src/memmgr.c \
../src/words.c 

OBJS += \
./src/DebugFileManager.o \
./src/DebugManager.o \
./src/Lab9RevisedWordCounterProject.o \
./src/Trim.o \
./src/WordCounterApp.o \
./src/linkedlist.o \
./src/memmgr.o \
./src/words.o 

C_DEPS += \
./src/DebugFileManager.d \
./src/DebugManager.d \
./src/Lab9RevisedWordCounterProject.d \
./src/Trim.d \
./src/WordCounterApp.d \
./src/linkedlist.d \
./src/memmgr.d \
./src/words.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


