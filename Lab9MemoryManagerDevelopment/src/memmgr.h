/**
 * This is the header file for the memory manager.  It defines the interface to the memory manager.
 */

#ifndef MEMMGR_H
#define MEMMGR_H

#include <stdint.h>

/**
 * The following structure is present in the header in order to allow testing to occur using the testMemMgr.cpp file.  Normally this would be private and not need to be exposed as it is really an internal data structure.
 */
typedef struct hdr {
	struct hdr *ptr;
	unsigned int size;
} memhdr_struct;

/**
 * This method will free the memory pointed to by the pointer.
 * @param ap This is the pointer to the data.
 */
void memmgr_free(void *ap);

/**
 * This method will malloc a continguous block of memory on the heap.
 * @param nbytes This is the number of bytes to allocate.
 * @return the Return will be a pointer to the allocated block of memory.
 */
void *memmgr_malloc(uint32_t nbytes);
/**
 * This method will initialize the heap.
 * @param pheapStart This is a pointer to the first byte in the heap.
 * @param pheapEnd This si a pointer to the last byte in the heap.
 */
void memmgr_init(void* pheapStart, void* pheapEnd);

/**
 * This method will return the number of fragments in memory.
 * @return The return will be the number of fragments in memory.
 */
int memmgr_get_number_of_fragments();
/**
 * This method will return the number of bytes currently allocated.
 * @return The number of bytes currently allocated will be returned.
 */
int memmgr_get_allocated_space();
/**
 * This method will return the remaining space which is not allocated.
 * @return The number of bytes which are unallocated will be returned.
 */
int memmgr_get_remaining_space();
/**
 * This method will return the maximum allocated space since initialization, which is the maximum number of bytes that have ever been allocated.
 * @return The maximum allocation will be returned.
 */
int memmgr_get_maximum_allocated_space();
/**
 * This method will return a count of the number of times that malloc has been invoked.
 * @return The return will be the number of times that malloc has been invoked.
 */
long int memmgr_get_malloc_count();
/**
 * This method will return the number of times free has been called.
 * @return The return will eb the number of free calls that have occurred.
 */
long int memmgr_get_free_count();
#endif
