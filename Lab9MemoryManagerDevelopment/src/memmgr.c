
#include <stdio.h>
#include "memmgr.h"


/* Defined in the linker file. _heapstart is the first byte allocated to the heap;
 _heapend is the last. */
static memhdr_struct *frhd;
static int memleft; /* memory left */
static int heapsize;	//heap size in bytes
static int maxallocatedspace;
static void* heapstart;
static void* heapend;
static long int mallocCount;
static long int freeCount;
static FILE* logFile;

static void setMaxAllocated();

/**
 * This method will free the memory pointed to by the pointer.
 * @param ap This is the pointer to the data.
 */
void memmgr_free(void *ap) {
	freeCount++;
	/*	Return memory to free list. Where possible, make contiguous blocks of free memory. (Assumes that 0 is not a valid
	 * address for allocation. Also, i_alloc() must be called prior to using either free() or malloc(); otherwise, the free list will be null.) */

	memhdr_struct *nxt, *prev, *f;
	f = (memhdr_struct *) ap - 1; /* Point to header of block being returned. */
	memleft += f->size;
	/* frhd is never null unless i_alloc() wasn't called to initialize package. */
	if (frhd > f) {
		/* Free-space head is higher up in memory than returnee. */
		nxt = frhd; /* old head */
		frhd = f; /* new head */
		prev = f + f->size; /* right after new head */
		if (prev == nxt) /* Old and new are contiguous. */
		{ /* Old and new are contiguous. */
			f->size += nxt->size;
			f->ptr = nxt->ptr; /* Form one block. */
		} else
			f->ptr = nxt;
		return;
	}

	/*	Otherwise, current free-space head is lower in memory. Walk down free-space list looking for the block being returned.
	 * If the next pointer points past the block, make a new entry and link it. If next pointer plus its size points to the block,
	 *  form one contiguous block. */
	nxt = frhd;
	for (nxt = frhd; nxt && nxt < f; prev = nxt, nxt = nxt->ptr) {
		if (nxt + nxt->size == f) {
			nxt->size += f->size; /* They're contiguous. */
			f = nxt + nxt->size; /* Form one block. */
			if (f == nxt->ptr) {

				/* The new, larger block is contiguous to the next free block, so form a larger block.There's no need to continue
				 * this checking since if the block following this free one were free, the two would already have been combined. */

				nxt->size += f->size;
				nxt->ptr = f->ptr;
			}
			return;
		}
	}

	/*	The address of the block being returned is greater than one in the free queue (nxt) or the end of the queue was reached.
	 *  If at end, just link to the end of the queue. Therefore, nxt is null or points to a block higher up in memory than the one being returned. */

	prev->ptr = f; /* link to queue */
	prev = f + f->size; /* right after space to free */
	if (prev == nxt) /* 'f' and 'nxt' are contiguous. */
	{
		f->size += nxt->size;
		f->ptr = nxt->ptr; /* Form a larger, contiguous block. */
	} else {
		f->ptr = nxt;
	}
	return;
}

/**
 * This method will malloc a contiguous block of memory on the heap.
 * @param nbytes This is the number of bytes to allocate.
 * @return the Return will be a pointer to the allocated block of memory.
 */
void* memmgr_malloc(uint32_t nbytes) /* bytes to allocate */
{
	memhdr_struct *nxt, *prev;
	int nunits;
	nunits = (nbytes + sizeof(memhdr_struct) - 1) / sizeof(memhdr_struct) + 1;

	//Assume the mallocCount passes. If it does not, decrement it before exiting.
	mallocCount++;

	/* Change that divide to a shift (for speed) only if the compiler doesn't do it for you, you don't require portability,
	 *  and you know that sizeof(memhdr_struct) is a power of two. Allocate the space requested plus space for the header of the block.
	 *   Search the free-space queue for a block that's large enough. If block is larger than needed, break into two pieces and allocate
	 *   the portion higher up in memory. Otherwise, just allocate the entire block. */

	for (prev = NULL, nxt = frhd; nxt; nxt = nxt->ptr) {
		if (nxt->size >= nunits) /* big enough */
		{
			if (nxt->size > nunits) {
				nxt->size -= nunits; /* Allocate tell end. */
				nxt += nxt->size;
				nxt->size = nunits; /* nxt now == pointer to be alloc'd. */
			} else {
				if (prev == NULL) {
					frhd = nxt->ptr;
				} else {
					prev->ptr = nxt->ptr;
				}
			}
			memleft -= nunits;

			/* Return a pointer past the header to the actual space requested. */
			setMaxAllocated();
			return ((char*) (nxt + 1));
		}
	}

	/* This function that explains what catastrophe befell us before resetting the system.
	 */
	mallocCount--;
	return NULL;
}

/**
 * This method will initialize the heap.
 * @param pheapStart This is a pointer to the first byte in the heap.
 * @param pheapEnd This si a pointer to the last byte in the heap.
 */
void memmgr_init(void* pheapStart, void* pheapEnd) {
	if (pheapStart != NULL && pheapEnd != NULL) {
		//initialize variables
		heapstart = pheapStart;
		heapend = pheapEnd;
		maxallocatedspace = mallocCount = freeCount = 0;
		heapsize = (heapend - heapstart);	//heap size in bytes

		frhd = pheapStart; /* Initialize the allocator. */
		frhd->ptr = NULL;
		frhd->size = ((char *) pheapEnd - (char *) pheapStart)
				/ sizeof(memhdr_struct);
		memleft = frhd->size; /* initial size in four-byte units */
	}
}

/**
 * This method will return the number of fragments in memory.
 * @return The return will be the number of fragments in memory.
 */
int memmgr_get_number_of_fragments(){
	memhdr_struct* nxt = frhd;
	int count = 0;
	while(nxt){
		nxt = nxt->ptr;
		count++;
	}
	return count - 1;
}
/**
 * This method will return the number of bytes currently allocated.
 * @return The number of bytes currently allocated will be returned.
 */
int memmgr_get_allocated_space(){
	//todo verify this works
	return heapsize - memmgr_get_remaining_space();
}
/**
 * This method will return the remaining space which is not allocated.
 * @return The number of bytes which are unallocated will be returned.
 */
int memmgr_get_remaining_space(){
	//memleft is number of memhdr_stucts that can be put on the heap.
	return memleft * sizeof(memhdr_struct);
}
/**
 * This method will return the maximum allocated space since initialization, which is the maximum number of bytes that have ever been allocated.
 * @return The maximum allocation will be returned.
 */
int memmgr_get_maximum_allocated_space(){
	//todo verify this works
	return maxallocatedspace;
}
/**
 * This method will return a count of the number of times that malloc has been invoked.
 * @return The return will be the number of times that malloc has been invoked.
 */
long int memmgr_get_malloc_count(){
	return mallocCount;
}
/**
 * This method will return the number of times free has been called.
 * @return The return will eb the number of free calls that have occurred.
 */
long int memmgr_get_free_count(){
	//todo verify this works
	return freeCount;
}

static void setMaxAllocated(){
	int allocated;
	if((allocated = memmgr_get_allocated_space()) > maxallocatedspace){
		maxallocatedspace = allocated;
	}
}
